
## initial requirements

### Usage

```
yarn install
yarn start
open http://localhost:3000
```

### Tasks

- Please add pagination support to the list when there are more than 2 entries
- Please add option to select sex of a friend male/female and display it
- Please add tests using your preferred testing tool (jest, mocha, should.js ...)

### Objectives

- You have received a working example so please do not upgrade any packages unless you really have to.
- Please check for small things like syntax errors, since details matter.
- Please deliver something that works, non working project is an automatic disqualification

## after changes

### Usage

```
npm ci
npm start
open http://localhost:3000
```

### Objectives response

- added pagination support
- added option to select sex to a friend male/female/other
- unit tested react state management

Didn't test reducers as it is mostly existing code.

#### modifications

I primarily use npm, so `yarn` parts were removed.

Upgraded react to use react hooks.

- `lodash`: was removed as not needed needed
- `react-scripts`: updated as it had vulnerabilities
- `prop-types`: `react 16` doesn't support prop types from `react 15`
- `jest`: for unit testing
- `@testing-library/react-hooks` and others: needed for hooks testing to work with react-script problems and injection avoidance

Restructured project as logic with separated to much and was difficult to make changes.
There were a lot of mines planted in the code.
First tests were not running, which was also not helpful as it was a dead code.