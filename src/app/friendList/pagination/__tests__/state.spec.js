import { usePaginationState } from '../state'
import { renderHook } from '@testing-library/react-hooks'

describe('Pagination.state', () => {
  it('can get value state out', () => {
    const input = 1

    const { result } = renderHook(() => usePaginationState(input))

    expect(result.current.page).toBe(input)
  });

  it('calculates total correctly', () => {
    const input = 1
    const perPage = 2
    const list = [...Array(3).keys()]

    const expected = 2

    const { result } = renderHook(() => usePaginationState(input, list, perPage))

    expect(result.current.total).toBe(expected)
    expect(result.current.list.length).toBe(perPage)
  });

  it('changes value', () => {
    const input = 1

    const expected = 2

    const { result } = renderHook(() => usePaginationState(input))

    result.current.onChange(expected)

    expect(result.current.page).toBe(expected)
  });
});
