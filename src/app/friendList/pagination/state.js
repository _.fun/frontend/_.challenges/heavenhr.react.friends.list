import { useState } from "react"

export const usePaginationState = (current, list = [], perPage = 1, ) => {
  const [page, setPage] = useState(current)

  const onChange = number => setPage(number)
  const total = Math.ceil(list.length / perPage)

  const result = list.slice((page - 1) * perPage, (page - 1) * perPage + perPage)

  return {
    page,
    total,
    list: result,
    onChange,
  }
}