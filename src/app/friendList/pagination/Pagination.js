import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import './styles.css'

export const Pagination = ({ page, total, onChange }) => (
  <div>
    {[...Array(total).keys()]
      .map(x => (x + 1))
      .map(x => (
        <button
          key={x}
          onClick={() => onChange(x)}
          className={classnames({
            'selected-page': page === x,
          })}
        >
          {x}
        </button>
      ))}
  </div>
)

Pagination.propTypes = {
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  onChange: PropTypes.func,
}

Pagination.defaultProps = {
  onChange: () => { },
}