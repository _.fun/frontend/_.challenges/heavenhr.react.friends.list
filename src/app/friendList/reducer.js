import * as types from './constants'

const initialState = {
  list: [
    {
      id: 0,
      name: 'Theodore Roosevelt',
      starred: true,
      sex: 'male',
    },
    {
      id: 1,
      name: 'Abraham Lincoln',
      starred: false,
      sex: 'male,'
    },
    {
      id: 2,
      name: 'George Washington',
      starred: false,
      sex: 'male',
    }
  ]
}

export const friendList = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_FRIEND:
      {
        return {
          ...state,
          list: [
            ...state.list,
            {
              id: state.list.length + 1,
              name: action.name,
              starred: true,
              sex: 'male',
            }
          ]
        }
      }

    case types.DELETE_FRIEND:
      {
        return {
          ...state,
          list: state.list.filter(({ id }) => id !== action.id),
        }
      }

    case types.STAR_FRIEND:
      {
        const friends = [...state.list]
        let friend = friends.find(({ id }) => id === action.id)
        friend.starred = !friend.starred
        return {
          ...state,
          list: friends,
        }
      }

    case types.CHANGE_SEX:
      {
        const friends = [...state.list]
        let friend = friends.find(({ id }) => id === action.id)
        friend.sex = action.sex
        return {
          ...state,
          list: friends,
        }
      }

    default: return state
  }
}
