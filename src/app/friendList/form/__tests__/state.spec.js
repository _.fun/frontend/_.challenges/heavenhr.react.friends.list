import { useInput } from '../state'
import { renderHook } from '@testing-library/react-hooks'

describe('Input.state', () => {
  it('can get value state out', () => {
    const mockOnSave = jest.fn(() => { })

    const input = 'something'

    const { result } = renderHook(() => useInput(input, mockOnSave))

    expect(result.current.value).toBe(input)
  });

  it('changes value', () => {
    const mockOnSave = jest.fn(() => { })

    const input = 'something'
    const expected = 'somethingElse'

    const { result } = renderHook(() => useInput(input, mockOnSave))

    result.current.onChange({ target: { value: expected } })

    expect(result.current.value).toBe(expected)
  });

  it('submits', () => {
    const mockOnSave = jest.fn(() => { })

    const input = 'something'
    const key = 13

    const { result } = renderHook(() => useInput(input, mockOnSave))

    result.current.onSubmit({ which: key })

    expect(mockOnSave).toBeCalledWith(input)
  });

  it('does not submit', () => {
    const mockOnSave = jest.fn(() => { })

    const input = 'something'
    const key = 12

    const { result } = renderHook(() => useInput(input, mockOnSave))

    result.current.onSubmit({ which: key })

    expect(mockOnSave).not.toBeCalled()
  });
});
