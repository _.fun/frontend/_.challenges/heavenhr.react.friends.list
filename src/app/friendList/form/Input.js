import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import styles from './styles.css'

import { useInput } from './state'

export const Input = ({ name = '', addFriend }) => {
  const { value, onChange, onSubmit } = useInput(name, addFriend)

  return (
    <input
      type="text"
      autoFocus={true}
      className={classnames('form-control', styles.addFriendInput)}
      placeholder="Type the name of a friend"
      value={value}
      onChange={onChange}
      onKeyDown={onSubmit}
    />
  )
}

Input.propTypes = {
  name: PropTypes.string,
  addFriend: PropTypes.func.isRequired
}

Input.defaultProp = {
  name: '',
}
