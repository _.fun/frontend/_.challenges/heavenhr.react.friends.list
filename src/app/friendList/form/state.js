import { useState } from 'react'

export const useInput = (input, onSave) => {
    const [value, setValue] = useState(input)

    const onChange = e => setValue(e.target.value)

    const onSubmit = e => {
        if (e.which !== 13) return

        onSave((value || '').trim())
        setValue('')
    }

    return { value, onChange, onSubmit }
}
