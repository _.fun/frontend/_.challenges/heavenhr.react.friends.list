import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.css'

import { Item } from './item'

export const List = ({ friends, actions }) => (
  <ul className={styles.friendList}>
    {friends.map((friend) => (
      <Item
        key={friend.id}
        friend={friend}
        {...actions}
      />
    ))}
  </ul>
)

List.propTypes = {
  friends: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}
