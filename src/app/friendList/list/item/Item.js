import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styles from './styles.css'

const sexes = [
  'male', 'female', 'other'
]

export const Item = ({ friend, starFriend, deleteFriend, changeFriendsSex }) => (
  <li className={styles.friendListItem}>
    <div className={styles.friendInfos}>
      <div>
        <span>{friend.name}</span>
      </div>
      <div>
        <small>xx friends in common</small>
      </div>
    </div>
    <div className={styles.friendActions}>
      <button
        className={`btn btn-default ${styles.btnAction}`}
        onClick={() => starFriend(friend.id)}
      >
        <i
          className={classnames('fa', {
            'fa-star': friend.starred,
            'fa-star-o': !friend.starred
          })}
        />
      </button>

      <select
        name="sex"
        value={friend.sex}
        onChange={(e) => changeFriendsSex(friend.id, e.target.value)}
      >
        {sexes.map(x => (<option key={x}>{x}</option>))}
      </select>

      <button
        className={`btn btn-default ${styles.btnAction}`}
        onClick={() => deleteFriend(friend.id)}
      >
        <i className="fa fa-trash" />
      </button>
    </div>
  </li>
)

Item.propTypes = {
  friend: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    sex: PropTypes.string.isRequired,
    starred: PropTypes.bool,
  }),
  starFriend: PropTypes.func.isRequired,
  changeFriendsSex: PropTypes.func.isRequired,
}
