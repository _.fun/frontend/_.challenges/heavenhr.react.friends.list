import React from 'react'
import { connect } from 'react-redux'
import styles from './styles.css'

import {
  addFriend as addFriendAction,
  deleteFriend as deleteFriendAction,
  starFriend as starFriendAction,
  changeFriendsSex as changeFriendsSexAction,
} from './actions'

import { Input } from './form'
import { List } from './list'
import { Pagination, usePaginationState } from './pagination'

const defaultStartingPage = 1
const perPage = 2

const FriendList = ({ friends, ...otherProps }) => {
  const { page, total, list, onChange } = usePaginationState(defaultStartingPage, friends, perPage)
  const actions = { ...otherProps }

  return (
    <div className={styles.friendListApp}>
      <h1>The FriendList</h1>
      <Input addFriend={actions.addFriend} />
      <List friends={list} actions={actions} />
      <Pagination page={page} total={total} onChange={onChange} />
    </div>
  )
}

const mapStateToProps = ({ friendList: { list } }) => ({
  friends: list,
})

export default connect(
  mapStateToProps,
  {
    addFriend: addFriendAction,
    deleteFriend: deleteFriendAction,
    starFriend: starFriendAction,
    changeFriendsSex: changeFriendsSexAction,
  }
)(FriendList)
