import * as types from './constants'

export function addFriend(name) {
  return {
    type: types.ADD_FRIEND,
    name
  }
}

export function deleteFriend(id) {
  return {
    type: types.DELETE_FRIEND,
    id
  }
}

export function starFriend(id) {
  return {
    type: types.STAR_FRIEND,
    id
  }
}

export const changeFriendsSex = (id, sex) => ({
  type: types.CHANGE_SEX,
  id,
  sex,
})
