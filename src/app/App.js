import React, { Component } from 'react'
import { combineReducers, createStore } from 'redux'
import { Provider } from 'react-redux'

import { FriendList } from './friendList'
import * as reducers from './reducers'

const reducer = combineReducers(reducers)
const store = createStore(reducer)

export class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <FriendList />
        </Provider>
      </div>
    )
  }
}
